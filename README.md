# no-internet-reboot

Reboot device (supposedly OpenWRT system/router) if there is no internet. Written in bash (so one should be installed).

Future versions shoud support ash/sh.


## Installation:

1) put checkInternet into /etc/init.d/ directory
2) enable it on startup using: /etc/init.d/checkInternet enable

disable service using:
/etc/init.d/checkInternet disable
